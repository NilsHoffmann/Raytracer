import kotlin.math.sqrt

fun reflect(direction: Vector3, surfaceNormal: Vector3): Vector3 {
    return direction - surfaceNormal * (direction dot surfaceNormal) * 2.0
}

interface Material {
    fun scatter(ray: Ray, collision: CollisionRecord): ScatterResult
}

data class ScatterResult(val hit: Boolean, val scatteredRay: Ray?, val albedo: Colour?) {
    constructor() : this(false, null, null)
}


class Lambertian(val albedo: Colour) : Material {

    override fun scatter(ray: Ray, collision: CollisionRecord): ScatterResult {
        val target = collision.point + collision.surfaceNormal + random_in_unit_sphere()
        val scatteredRay = Ray(collision.point, target - collision.point)
        return ScatterResult(true, scatteredRay, albedo)
    }
}

class Metal(val albedo: Colour, val fuzziness: Double = 1.0) : Material {
    override fun scatter(ray: Ray, collision: CollisionRecord): ScatterResult {
        val reflectionDir = reflect(ray.direction.getUnitVector(), collision.surfaceNormal) +
                random_in_unit_sphere() * fuzziness
        val scattered: Ray = Ray(collision.point, reflectionDir)
        if (scattered.direction dot collision.surfaceNormal > 0) {
            return ScatterResult(true, scattered, albedo)
        } else return ScatterResult()
    }
}

class Dielectric(val attenuation: Colour = Colour(1.0, 1.0, 1.0)): Material {

    fun refract(inGoing: Vector3, normal: Vector3, refIdx: Double): Vector3? {
        val unitIn = inGoing.getUnitVector()
        val dt = inGoing.getUnitVector() dot normal
        val discriminant = 1.0 - refIdx * refIdx * (1.0-dt*dt)
        if (discriminant > 0){
            return (unitIn * normal * dt) * refIdx - normal * sqrt(discriminant)
        }
        else return null
    }

    override fun scatter(ray: Ray, collision: CollisionRecord): ScatterResult {
        val reflected = reflect(ray.direction, collision.surfaceNormal)

        if(ray.direction dot collision.surfaceNormal > 0){
            val outwards_normal = -collision.surfaceNormal
        }
        return ScatterResult()
    }
}
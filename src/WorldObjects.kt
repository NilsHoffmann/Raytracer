import kotlin.math.sqrt

interface Collidable {
    fun collide(ray: Ray, tmin: Double, tmax: Double): CollisionRecord
}


class Sphere(val center: Vector3, val radius: Double, val material: Material):Collidable {
    override fun collide(ray: Ray, tmin: Double, tmax: Double): CollisionRecord {
        val originCenter = ray.origin - center
        val a = ray.direction dot ray.direction
        val b = originCenter dot ray.direction
        val c = (originCenter dot originCenter) - radius * radius
        val discriminat = b * b - a * c
        if (discriminat > 0) {
            val t = (-b - sqrt(discriminat)) / a
            if (tmin < t && t <= tmax) {
                val point = ray.pointAtValue(t)
                val surfaceNormal = (point - center).getUnitVector()
                return CollisionRecord(true, t, point, surfaceNormal, material)
            }
            //second solution more far away
            val t2 = (-b + sqrt(discriminat)) / a
            if (tmin < t2 && t2 <= tmax) {
                val point = ray.pointAtValue(t2)
                val surfaceNormal = (point - center) / radius
                return CollisionRecord(true, t2, point, surfaceNormal, material)
            }
        }
        return CollisionRecord()
    }
}
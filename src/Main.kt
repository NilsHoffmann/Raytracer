import java.io.File
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt

fun main(args: Array<String>) {

    val len_x = 800
    val len_y = 400
    val n_samples = 100

    //add a few world objects
    World.worldObjects.add(
            Sphere(Vector3(0.0, 0.0, -1.0), 0.5,
                    Metal(Colour(0.8, 0.8, 0.8), fuzziness = 0.1)))
    World.worldObjects.add(
            Sphere(Vector3(0.0, -100.5, -1.0), 100.0,
                    Lambertian(Colour(0.8, 0.3, 0.3))))

    val camera = Camera()
    val buffer = Framebuffer(len_x, len_y)
    for (j in (0 until len_y).reversed()) {
        for (i in 0 until len_x) {
            var colour = Vector3()
            for (c in 0 until n_samples) {
                val u = (i + Math.random()) / len_x
                val v = (j + Math.random()) / len_y
                val r = camera.getRay(u, v)
                colour += colour(r, 0)
            }
            colour /= n_samples.toDouble()
            buffer[i, j] = colour.asColour()
        }
    }

    buffer.writePPM("image.ppm")
}

fun colour(ray: Ray, recursionDepth: Int): Colour {
    val c = World.calcCollisions(ray, 0.001, Double.MAX_VALUE)
    if (c.collision) {
        val scattered = c.material!!.scatter(ray, c)
        if (scattered.hit && recursionDepth < 50) {
            return (scattered.albedo!! * colour(scattered.scatteredRay!!, recursionDepth + 1)).asColour()
        } else return Colour(0.0, 0.0, 0.0)
    }
    return colourBackground(ray)
}


fun colourBackground(ray: Ray): Colour {
    val unitDirection = ray.direction.getUnitVector()
    val t = 0.5 * (unitDirection.y + 1)
    return (Colour(1.0, 1.0, 1.0) * (1.0 - t) + Colour(0.5, 0.7, 1.0) * t).asColour()
}

class Camera {
    val origin = Vector3(0.0, 0.0, 0.0)
    val horizontal = Vector3(4.0, 0.0, 0.0)
    val vertical = Vector3(0.0, 2.0, 0.0)
    val lowerLeftCorner = Vector3(-2.0, -1.0, -1.0)

    fun getRay(u: Double, v: Double): Ray {
        return Ray(origin, lowerLeftCorner + horizontal * u + vertical * v)
    }
}

fun random_in_unit_sphere(): Vector3 {
    var p: Vector3
    do {
        p = Vector3(Math.random(), Math.random(), Math.random()) * 2.0 - Vector3(1, 1, 1)
    } while (p.length > 1)
    return p
}

open class Vector3(val x: Double, val y: Double, val z: Double) : Sequence<Double> {
    constructor(x: Int, y: Int, z: Int) : this(x.toDouble(), y.toDouble(), z.toDouble())
    constructor() : this(0, 0, 0)

    override fun iterator(): Iterator<Double> {
        return listOf(x, y, z).iterator()
    }

    operator fun get(i: Int) {
        when (i) {
            0 -> x
            1 -> y
            2 -> z
            else -> throw IndexOutOfBoundsException()
        }
    }


    fun getUnitVector(): Vector3 {
        return this.scaleTo(1.0)
    }

    fun scaleTo(newLen: Double): Vector3 {
        val (n1, n2, n3) = this.map { e -> (newLen / length) * e }.toList()
        return Vector3(n1, n2, n3)
    }


    val length: Double
        get() = sqrt(this.map { e -> e.pow(2) }.sum())

    operator fun plus(other: Vector3) = Vector3(x + other.x, y + other.y, z + other.z)
    operator fun minus(other: Vector3) = Vector3(x - other.x, y - other.y, z - other.z)
    operator fun times(k: Double) = Vector3(x * k, y * k, z * k)
    operator fun times(other: Vector3) = Vector3(x * other.x, y * other.y, z * other.z)
    operator fun div(k: Double) = Vector3(x / k, y / k, z / k)
    operator fun unaryMinus() = Vector3(-x, -y, -z)

    infix fun dot(other: Vector3): Double {
        return x * other.x + y * other.y + z * other.z
    }

    fun asColour(): Colour {
        return Colour(x, y, z)
    }
}

class Colour(val r: Double, val g: Double, val b: Double) : Vector3(r, g, b) {
    constructor() : this(0.0, 0.0, 0.0)
    constructor(c: Vector3) : this(c.x, c.y, c.z)

    fun asFracString(of: Int): String {
        val gc = Vector3(sqrt(r), sqrt(g), sqrt(b)).asColour()
        return "${(gc.r * of).roundToInt()} ${(gc.g * of).roundToInt()} ${(gc.b * of).roundToInt()}\n"
    }
}


class Ray(val origin: Vector3, val direction: Vector3) {
    fun pointAtValue(k: Double): Vector3 {
        return (origin + direction * k)
    }
}

class Framebuffer(val len_x: Int, val len_y: Int) {
    val buffer = Array(len_x) { Array(len_y) { Colour() } }
    operator fun get(x: Int, y: Int): Colour {
        return buffer[x][y]
    }


    fun writePPM(filename: String) {
        File(filename).printWriter().use {
            it.print("P3\n")
            it.print("$len_x $len_y\n")
            it.print("255\n")
            for (j in (0 until len_y).reversed()) {
                for (i in 0 until len_x) {
                    it.print(this[i, j].asFracString(255))
                }
            }
        }
    }

    operator fun set(x: Int, y: Int, value: Colour) {
        buffer[x][y] = value
    }
}

data class CollisionRecord(val collision: Boolean, val t: Double, val point: Vector3,
                           val surfaceNormal: Vector3, val material: Material?) {
    constructor() : this(false, Double.MAX_VALUE, Vector3(), Vector3(), null)
}


object World {
    var worldObjects = ArrayList<Collidable>()
    fun calcCollisions(ray: Ray, tmin: Double, tmax: Double): CollisionRecord {
        var nearestCollision = CollisionRecord()
        for (item in worldObjects) {
            val collision = item.collide(ray, tmin, nearestCollision.t)
            if (collision.collision) nearestCollision = collision
        }
        return nearestCollision
    }
}

